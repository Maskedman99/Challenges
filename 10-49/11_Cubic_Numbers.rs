// https://dev.to/thepracticaldev/daily-challenge-11-cubic-numbers-21am

/*  Create a method that can determine whether a number is a cubic number or not. 
    If the number has been determined to not be cubic, the output should be either null or the string "Unlucky."
    Cubic numbers are numbers with at most three digits, such that the sum of the cubes of their digits is the number itself.
    For example: 153 is a cubic number, because 1^3 + 5^3 + 3^3 = 153. 
*/

use std::io;

fn main() {
    let mut input_line = String::new();

    io::stdin().read_line(&mut input_line).expect("Failed to read line");
    let n: u32 = input_line.trim().parse().expect("Input not an integer");

    let mut x = n;
    
    let mut s = 0;
    while x > 0 {
        s += u32::pow(x%10, 3);
        x = x/10;
    }

    if s == n {
        println!("Lucky")
    }
    else {
        println!("Unlucky")
    }
}

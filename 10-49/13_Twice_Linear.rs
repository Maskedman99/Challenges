// https://dev.to/thepracticaldev/daily-challenge-13-twice-linear-4pi9

/*  Consider a sequence u, where u is defined as follows:

    The number u(0) = 1 is the first one in u.
    For each x in u, y = 2 * x + 1 and z = 3 * x + 1 must also be in u.
    There are no other numbers in u.

    Ex: u = [1, 3, 4, 7, 9, 10, 13, 15, 19, 21, 22, 27, ...]
    1 gives 3 and 4. 3 gives 7 and 10 while 4 gives 9 and 13. This pattern continues as far you as allow it to.

    Your task is to create a function dbl_linear with a parameter of n that returns the element u(n) in an ordered 
    sequence (excluding duplicates).
*/

use std::io;
use std::io::Write;

fn main() {
    let mut input_line = String::new();
    
    print!("Enter the first element: ");
    io::stdout().flush().unwrap();
    io::stdin().read_line(&mut input_line).expect("Failed to read line");
    let u: u32 = input_line.trim().parse().expect("Input not an integer");

    print!("Enter Limit: ");
    io::stdout().flush().unwrap();
    input_line.clear();
    io::stdin().read_line(&mut input_line).expect("Failed to read line");
    let n: u32 = input_line.trim().parse().expect("Input not an integer");

    println!("{:?}", dbl_linear(u, n));
}

fn dbl_linear(u: u32, n: u32)-> Vec<u32> {
    let mut a: Vec<u32> = Vec::new();
    a.push(u);

    for i in 0..n as usize {
        a.push(2*a[i] + 1);
        a.push(3*a[i] + 1);

        a.sort();
    }       
    
    return a;
}

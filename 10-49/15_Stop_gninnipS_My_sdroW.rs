// https://dev.to/thepracticaldev/daily-challenge-15-stop-gninnips-my-sdrow-255j

/*
    Write a function that takes in a string of one or more words and returns the same string, 
    but with all words with five letters or more reversed. 
    Strings passed in will consist of only letters and spaces.
*/

use std::io;
use std::io::Write;

fn main() {
    let mut line = String::new();
    print!("Enter the string: ");
    io::stdout().flush().unwrap();
    io::stdin().read_line(&mut line).expect("Failed to read line");

    // avoid '/n' in 'line' by excluding the last element
    let a: Vec<&str> = line[0..line.len()-1].split(' ').collect::<Vec<&str>>();
    for (_pos, e) in a.iter().enumerate() {
        if e.len() >= 5 {
            print!("{} ", e.chars().rev().collect::<String>());
        } else {
            print!("{} ", e);
        }
        io::stdout().flush().unwrap();
    }

    // to prevent outputing an unwanted charector (related to flushing buffer)
    println!("");
}

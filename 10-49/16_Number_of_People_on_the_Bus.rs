// https://dev.to/thepracticaldev/daily-challenge-16-number-of-people-on-the-bus-dik

/*
    There is a bus moving in the city, and it takes and drop some people at each bus stop.
    You are provided with a list (or array) of integer arrays (or tuples). 
    Each integer array has two items which represent the number of people that get on the bus (The first item) 
    and the number of people that get off the bus (The second item) at a bus stop.

    Your task is to return number of people who are still left on the bus after the last bus station (after the last array).
*/

use::std::io;
use::std::io::Write;

fn main() {
    let mut input_line: String = String::new();

    print!("Enter the no.of stations: ");
    io::stdout().flush().unwrap();
    io::stdin().read_line(&mut input_line).expect("Failed to read input");
    let no_of_stations: usize = input_line.trim().parse().expect("Input not an integer");

    let mut a = vec![[0; 2]; no_of_stations]; 

    for i in 0..no_of_stations as usize {
        print!("Enter the no.of people that got in bus at station {}: ", i+1);
        io::stdout().flush().unwrap();
        input_line.clear();
        io::stdin().read_line(&mut input_line).expect("Failed to read input");
        a[i][0] = input_line.trim().parse().expect("Input not an integer");

        print!("Enter the no.of people that got out of bus at station {}: ", i+1);
        io::stdout().flush().unwrap();
        input_line.clear();
        io::stdin().read_line(&mut input_line).expect("Failed to read input");
        a[i][1] = input_line.trim().parse().expect("Input not an integer");
    }

    let mut count = 0;

    for i in 0..no_of_stations as usize {
        count += a[i][0];
        count -= a[i][1];
    }

    println!("\nThe no.of people still left in the bus: {}", count);
}

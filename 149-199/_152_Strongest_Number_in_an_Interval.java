// https://dev.to/thepracticaldev/daily-challenge-152-strongest-number-in-an-interval-pj6

/*
	The strength of an even number is determined by the amount of times we can successfully divide by 2 
 	until we reach an odd number.

	For example, if n = 12, then:
	12 / 2 = 6
	6 / 2 = 3
	We have divided 12 in half twice and reached 3, so the strength of 12 is 2.
	
	Given a closed interval [n, m], return the strongest even number in the interval. 
	If multiple solutions exist, return the smallest of the possible solutions.
	
	for [1, 2], return 2. (1 has a strength of 0; 2 has a strength of 1)
	for [5, 10], return 8. 
 */

import java.util.Scanner;

public class _152_Strongest_Number_in_an_Interval {
	public static void main(String[] args) {
		
		int limit1 = 0;
		int limit2 = 0;
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the Lower Limit: ");
		limit1 = sc.nextInt();
		System.out.print("Enter the Upper Limit: ");
		limit2 = sc.nextInt();
		sc.close();
		
		int max = 0;
		int maxStrength = 0;
		for(int i=limit1; i<=limit2; i++) {
			if(i%2 == 0) {
				int strength = 0;
				for(int j=i; j%2==0 && j!=0; j=j/2)
					strength += 1;
				if(strength > maxStrength) {
					maxStrength = strength;
					max = i;
				}
			}
		}
		
		System.out.println(max);
	}
}
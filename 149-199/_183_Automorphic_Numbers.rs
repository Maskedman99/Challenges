// https://dev.to/thepracticaldev/daily-challenge-183-automorphic-numbers-3mmj

/*
    For this challenge, implement a function that will return true if a number is Automorphic.
    Automorphics are numbers whose square ends in the same digits as the number itself.
    The number will always be positive.

    Examples
        autoMorphic(13) => false
        13 squared is 69. Since 69 does not end in 13, return false.

        autoMorphic(25) => true
        25 squared is 625. Since 625 ends in 25, return true.
*/

use std::io;
use std::io::Write;

fn main() {
    let mut input_line = String::new();

    print!("Enter the number: ");
    io::stdout().flush().unwrap();
    io::stdin().read_line(&mut input_line).expect("Failed to read input line");
    let number: u64 = input_line.trim().parse().expect("Input not an Integer");

    println!("{:?}", auto_morphic(number));
}

fn auto_morphic(number: u64) -> bool {
    if (number*number).to_string().ends_with(&number.to_string()) {
        return true;
    }
    return false;
}

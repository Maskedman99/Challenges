// https://dev.to/thepracticaldev/daily-challenge-169-christmas-tree-4dea

/*
    Create a function christmasTree(height) or christmas_tree(height) that returns a Christmas tree of the correct height
    christmasTree(5) || christmas_tree(height) should return:
            *
           ***
          *****
         *******
        *********

    Height passed is always an integer between 0 and 100.
    Use \n for newlines between each line.
    Pad with spaces so each line is the same length. The last line having only stars, no spaces.

 */

use std::io;
use std::io::Write;

fn main() {
    let mut input_line = String::new();

    print!("Enter the height of the christmas tree (1 - 100): ");
    io::stdout().flush().unwrap();
    io::stdin().read_line(&mut input_line).expect("Failed to read input line");
    let height: u8 = input_line.trim().parse().expect("Input not an Integer");

    christmas_tree(height);
}

fn christmas_tree(height: u8) {
    for i in 0..height {
        for _j in (0..(height-i-1)).rev() {
            print!(" ");
        }
        for _j in 0..(1 + i*2) {
            print!("*");
        }
        print!("\n");
    }
    io::stdout().flush().unwrap();
}
